package unicredit.gdpr;

import com.acplab.documents.CorpusWriter;
import com.acplab.documents.Document;
import com.acplab.documents.DocumentFactory;
import com.acplab.documents.DocumentFilter;
import com.acplab.documents.IDocument;
import com.acplab.ontology.Ontology;
import com.acplab.ontology.XMLFileOntologyBuilder;
import com.acplab.utils.xml.XMLDocument;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.PropertyConfigurator;
import org.jdom2.Element;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class CryptoLogic {

  private static final Logger LOGGER = Logger.getLogger(CryptoLogic.class);
  private static final int PASSWORD_LENGTH = 16;
  private static final Pattern STRING = Pattern.compile("(\\r|\\n|\\r\\n)+");
  private static final Iterable<String> PERSONAL_DATA_ATTRIBUTES = new ArrayList<>(Arrays.asList("Buyer CMN DESC", "Head of BL / SL / Gov", "BL / SL / Gov"));
  private static final String TOKEN = "4jK!@9g#rTx==19p";
  private static final String RIGHT_TO_BE_FORGOTTEN = "Deleted due to the GDPR right to be forgotten";
  private static final String BATCH_PATH = new File(CryptoLogic.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile().getPath();
  private static final String CONF_PATH = BATCH_PATH.concat(File.separator).concat("conf");

  static{
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
    System.setProperty("current.date", dateFormat.format(new Date()));
  }

  public static void main(final String[] args){
    System.out.println("GDPR tool started");
    setRelPathProp();
    final XMLDocument xml = new XMLDocument(CONF_PATH.concat(File.separator).concat("conf.xml"));
    LOGGER.info("Reading parameters from the configuration file " + xml.getFileName());
    final Element rootElement = xml.getDocument().getRootElement();
    if (rootElement == null){
      throw new IllegalStateException("GDPR parameters not found in the configuration file:" + xml.getFileName());
    }

    final Element ontoElement = rootElement.getChild("ontology");
    final Element encryptionElement = rootElement.getChild("encryption");
    final Element decryptionElement = rootElement.getChild("decryption");
    final Element deletionElement = rootElement.getChild("deletion");

    checkParameters(xml, ontoElement, encryptionElement, decryptionElement, deletionElement);

    final Element inputEncryptionElement = encryptionElement.getChild("input");
    final Element outputEncryptionElement = encryptionElement.getChild("output");
    final Element inputDecryptionElement = decryptionElement.getChild("input");
    final Element outputDecryptionElement = decryptionElement.getChild("output");
    final Element inputDeletionElement = deletionElement.getChild("input");
    final Element outputDeletionElement = deletionElement.getChild("output");

    checkInputOutputParams(inputEncryptionElement, outputEncryptionElement, inputDecryptionElement, outputDecryptionElement, inputDeletionElement, outputDeletionElement);

    final String valueLabel = "path";
    final String ontoPath = ontoElement.getAttributeValue(valueLabel);
    final String encryptInputPath = inputEncryptionElement.getAttributeValue(valueLabel);
    final String encryptOutputPath = outputEncryptionElement.getAttributeValue(valueLabel);
    final String decryptInputPath = inputDecryptionElement.getAttributeValue(valueLabel);
    final String decryptOutputPath = outputDecryptionElement.getAttributeValue(valueLabel);
    final String deletionInputPath = inputDeletionElement.getAttributeValue(valueLabel);
    final String deletionOutputPath = outputDeletionElement.getAttributeValue(valueLabel);

    checkPaths(ontoPath, encryptInputPath, encryptOutputPath, decryptInputPath, decryptOutputPath, deletionInputPath, deletionOutputPath);

    final Scanner scanner = new Scanner(System.in);
    System.out.print("Enter the modality encrypt/decrypt/delete: ");
    String mode;
    do {
      mode = scanner.next();
      if(!"encrypt".equalsIgnoreCase(mode) && !"decrypt".equalsIgnoreCase(mode) && !"delete".equalsIgnoreCase(mode)){
        System.out.print("Modality not valid, please enter encrypt, decrypt or delete mode: ");
      }
    } while (!"encrypt".equalsIgnoreCase(mode) && !"decrypt".equalsIgnoreCase(mode) && !"delete".equalsIgnoreCase(mode));

    try {
      LOGGER.info("Reading ontology path " + ontoPath);
      final XMLFileOntologyBuilder ob = new XMLFileOntologyBuilder(new File(ontoPath).getAbsolutePath());
      final Ontology ontology = ob.getOntology();
      switch(mode){
        case "encrypt":
          encryptCorpora(ontology, encryptInputPath, encryptOutputPath);
          break;
        case "decrypt":
          decryptCorpora(ontology, decryptInputPath, decryptOutputPath);
          break;
        case "delete":
          deleteFromCorpora(ontology, deletionInputPath, deletionOutputPath);
          break;
        default:
          throw new IllegalStateException("Unexpected value: " + mode);
      }
    } catch (final IOException e) {
      LOGGER.error(e.toString());
      LOGGER.info("Ontology not found!");
      System.out.print("Error: ontology not found or corrupted ontology file");
    }
  }

  private static void setRelPathProp() {
    try {
      final Properties props = new Properties();
      props.put("REL_PATH", BATCH_PATH.concat(File.separator).concat("log"));
      final InputStream in = new FileInputStream(CONF_PATH.concat(File.separator).concat("log4j.properties"));
      props.load(in);
      PropertyConfigurator.configure(props);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private static void decryptCorpora(final Ontology ontology, final String decryptInputPath, final String decryptOutputPath) {
    System.out.println("Reading corpora inside decryption input directory " + decryptInputPath);
    LOGGER.info("Reading decryption input directory path " + decryptInputPath);
    LOGGER.info("Reading decryption output directory path " + decryptOutputPath);
    if(checkIfFolderIsEmpty(decryptOutputPath)){
      try {
        final String password = readInputPassword();
        if(validateToken(password)){
          final byte[] key = password.getBytes();
          final Cipher cipher = Cipher.getInstance("AES");
          final Key secretKeySpeckey = new SecretKeySpec(key, "AES");
          cipher.init(Cipher.DECRYPT_MODE, secretKeySpeckey);
          final BASE64Decoder base64Decoder = new BASE64Decoder();
          final Stream<Path> paths = Files.walk(Paths.get(decryptInputPath));
          paths
              .filter(Files::isRegularFile)
              .map(Path::toFile)
              .filter(CryptoLogic::checkFileExtension)
              .forEach(file -> decrypt(file, cipher, base64Decoder, ontology, decryptOutputPath));
          LOGGER.info("Decrypting operation successfully completed");
          System.out.println("Decrypting operation successfully completed");
          System.out.println("Decrypted corpora are available at path " + decryptOutputPath);
        } else {
          System.out.println("Decrypting operation interrupted");
          LOGGER.info("Decrypting operation interrupted");
        }
      } catch (final NoSuchPaddingException | NoSuchAlgorithmException | IOException | InvalidKeyException e) {
        System.out.println("Decrypting operation failed");
        LOGGER.error(e.toString());
        LOGGER.info("Decrypting operation failed");
      }
    } else {
      System.out.println("Decrypt output folder " + decryptOutputPath + " is not empty, please clean the folder before encrypting corpora");
      LOGGER.info("Decrypt output folder : " + decryptOutputPath + " is not empty, please clean the folder before encrypting corpora");
    }
  }

  private static void decrypt(final File file, final Cipher cipher, final BASE64Decoder base64Decoder, final Ontology ontology, final String decryptOutputPath){
    LOGGER.info("Now elaborating corpus " + file.getName());
    System.out.println("Now elaborating corpus " + file.getName());
    final ArrayList<IDocument> docs = new ArrayList<IDocument>();
    try{
      final CorpusWriter corpusWriter = new CorpusWriter(String.valueOf(Paths.get(decryptOutputPath, file.getName())));
      DocumentFactory.openXmlCorpus(new File(file.getAbsolutePath()), ontology, new DocumentFilter() {
        @Override public void applyTo(final Document document) {
          for (final String personalDataAttribute: PERSONAL_DATA_ATTRIBUTES){
            final String personalDataToDecrypt = document.getIMetadata().get(personalDataAttribute, "");
            if (!personalDataToDecrypt.isEmpty()) {
              try {
                final byte[] base64decodedTokenArr = base64Decoder.decodeBuffer(personalDataToDecrypt);
                final byte[] messageDecrypted = cipher.doFinal(base64decodedTokenArr);
                document.getIMetadata().put(personalDataAttribute, new String(messageDecrypted));
              } catch (final IOException | IllegalBlockSizeException | BadPaddingException e) {
                throw new IllegalStateException(e);
              }
            }
          }
          docs.add(document);
        }
      });
      corpusWriter.write(docs);
      corpusWriter.close();
    } catch (final IOException e) {
      LOGGER.info("Decrypting operation failed");
      throw new IllegalStateException(e);
    }
    System.out.println("Completed elaboration of corpus " + file.getName());
    LOGGER.info("Completed elaboration of corpus " + file.getName());
  }

  private static void encryptCorpora(final Ontology ontology, final String encryptInputPath, final String encryptOutputPath) {
    System.out.println("Reading corpora inside encryption input directory " + encryptInputPath);
    LOGGER.info("Reading encryption input directory path " + encryptInputPath);
    LOGGER.info("Reading encryption output directory path " + encryptOutputPath);
    if(checkIfFolderIsEmpty(encryptOutputPath)){
      try {
        final String password = readInputPassword();
        storeToken(password);
        final byte[] key = password.getBytes();
        final Cipher cipher = Cipher.getInstance("AES");
        final Key secretKeySpeckey = new SecretKeySpec(key, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpeckey);
        final BASE64Encoder base64Encoder = new BASE64Encoder();
        final Stream<Path> paths = Files.walk(Paths.get(encryptInputPath));
        paths
            .filter(Files::isRegularFile)
            .map(Path::toFile)
            .filter(CryptoLogic::checkFileExtension)
            .forEach(file -> encrypt(file, cipher, base64Encoder, ontology, encryptOutputPath));
        LOGGER.info("Encrypting successfully completed");
        System.out.println("Encrypting operation successfully completed");
        System.out.println("Encrypted corpora are available at path " + encryptOutputPath);
      } catch (final NoSuchPaddingException | NoSuchAlgorithmException | IOException | InvalidKeyException e) {
        System.out.println("Encrypting operation failed");
        LOGGER.error(e.toString());
        LOGGER.info("Encrypting operation failed");
      }
    } else {
      System.out.println("Encrypt output folder " + encryptOutputPath + " is not empty, please clean the folder before encrypting corpora");
      LOGGER.info("Encrypt output folder " + encryptOutputPath + " is not empty, please clean the folder before encrypting corpora");
    }
  }

  @NotNull private static String readInputPassword() {
    String password;
    final Scanner scanner = new Scanner(System.in);
    System.out.print("Enter the 16 characters password: ");
    do {
      password = scanner.next().trim();
      if(password.length() != PASSWORD_LENGTH){
        System.out.print("Password not valid, please insert a 16 characters password: ");
      }
    } while (password.length() != PASSWORD_LENGTH);
    return password;
  }

  @NotNull private static String readInputName() {
    final Scanner scanner = new Scanner(System.in);
    System.out.print("Enter the personal information to delete: ");
    return scanner.nextLine();
  }

  private static void storeToken(final String password) {
    final File tokenFile = new File(String.valueOf(Paths.get(CONF_PATH, "token.txt")));
    try {
      final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tokenFile));
      final String base64encodedTokenArr = encryptToken(password);
      bufferedWriter.write(base64encodedTokenArr);
      bufferedWriter.close();
    } catch (final IOException e) {
      LOGGER.error(e.toString());
      throw new IllegalStateException(e.toString());
    }
  }

  private static boolean validateToken(final String password) {
    final File tokenFile = new File(String.valueOf(Paths.get(CONF_PATH, "token.txt")));
    try {
      final BufferedReader bufferedReader = new BufferedReader(new FileReader(tokenFile));
      final String token = bufferedReader.readLine();
      bufferedReader.close();
      final String base64encodedTokenArr = encryptToken(password);
      if(!token.equalsIgnoreCase(base64encodedTokenArr)){
        System.out.println("WARNING! The inserted password is different from the last password used to encrypt!");
        LOGGER.info("WARNING! The inserted password is different from the last password used to encrypt!");
        final Scanner scanner = new Scanner(System.in);
        System.out.print("Do you still want to proceed decrypting with the current password? [yes, no]: ");
        String proceed;
        do {
          proceed = scanner.next().trim();
          if(!"no".equalsIgnoreCase(proceed) && !"yes".equalsIgnoreCase(proceed)){
            System.out.print("Please insert 'yes' or 'no': ");
          }
        } while (!"no".equalsIgnoreCase(proceed) && !"yes".equalsIgnoreCase(proceed));
        switch (proceed){
          case "no":
            return false;
          case "yes":
            return true;
          default:
            throw new IllegalStateException("Unexpected value: " + proceed);
        }
      }
    } catch (final IOException e) {
      LOGGER.error(e.toString());
      throw new IllegalStateException(e.toString());
    }
    return true;
  }

  private static String encryptToken(final String password){
    String base64encodedTokenArr;
    try {
      final byte[] key = password.getBytes();
      final Cipher cipher = Cipher.getInstance("AES");
      final Key secretKeySpeckey = new SecretKeySpec(key, "AES");
      cipher.init(Cipher.ENCRYPT_MODE, secretKeySpeckey);
      final BASE64Encoder base64Encoder = new BASE64Encoder();
      final byte[] messageEncrypted;
      messageEncrypted = cipher.doFinal(TOKEN.getBytes());
      base64encodedTokenArr = base64Encoder.encodeBuffer(messageEncrypted);
      base64encodedTokenArr = STRING.matcher(base64encodedTokenArr).replaceAll("");
    } catch (final IllegalBlockSizeException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | BadPaddingException e) {
      throw new IllegalStateException(e.toString());
    }
    return base64encodedTokenArr;
  }

  private static boolean checkFileExtension(final File file) {
    return "corpus".equalsIgnoreCase(FilenameUtils.getExtension(file.getName()));
  }

  private static void encrypt(final File file, final Cipher cipher, final BASE64Encoder base64Encoder, final Ontology ontology, final String encryptOutputPath){
    System.out.println("Now elaborating corpus " + file.getName());
    LOGGER.info("Now elaborating corpus " + file.getName());
    final ArrayList<IDocument> docs = new ArrayList<IDocument>();
    try {
      final CorpusWriter corpusWriter = new CorpusWriter(String.valueOf(Paths.get(encryptOutputPath, file.getName())));
      DocumentFactory.openXmlCorpus(new File(file.getAbsolutePath()), ontology, new DocumentFilter() {
        @Override public void applyTo(final Document document) {
          for (final String personalDataAttribute: PERSONAL_DATA_ATTRIBUTES) {
            final String personalDataToEncrypt = document.getIMetadata().get(personalDataAttribute, "");
            if (!personalDataToEncrypt.isEmpty()) {
              try {
                final byte[] messageEncrypted = cipher.doFinal(personalDataToEncrypt.getBytes());
                String base64encodedTokenArr = base64Encoder.encodeBuffer(messageEncrypted);
                base64encodedTokenArr = STRING.matcher(base64encodedTokenArr).replaceAll("");
                document.getIMetadata().put(personalDataAttribute, base64encodedTokenArr);
              } catch (final IllegalBlockSizeException | BadPaddingException e) {
                throw new IllegalStateException(e);
              }
            }
          }
          docs.add(document);
        }
      });
      corpusWriter.write(docs);
      corpusWriter.close();
    } catch (final IOException e) {
      LOGGER.info("Encrypting operation failed");
      throw new IllegalStateException(e);
    }
    System.out.println("Completed elaboration of corpus " + file.getName());
    LOGGER.info("Completed elaboration of corpus " + file.getName());
  }

  private static void deleteFromCorpora(final Ontology ontology, final String deletionInputPath, final String deletionOutputPath) {
    System.out.println("Reading corpora inside deletion input directory " + deletionInputPath);
    LOGGER.info("Reading deletion input directory path " + deletionInputPath);
    LOGGER.info("Reading deletion output directory path " + deletionOutputPath);
    if(checkIfFolderIsEmpty(deletionOutputPath)){
      try {
          final String buyerNameToDelete = readInputName();
          final ReportGenerationLogic report = new ReportGenerationLogic();
          final Stream<Path> paths = Files.walk(Paths.get(deletionInputPath));
          paths
              .filter(Files::isRegularFile)
              .map(Path::toFile)
              .filter(CryptoLogic::checkFileExtension)
              .forEach(file -> delete(file, buyerNameToDelete, report, ontology, deletionOutputPath));
          LOGGER.info("Deletion operation successfully completed");
          System.out.println("Delete operation successfully completed");
          System.out.println("Cleaned corpora are available at path " + deletionOutputPath);
          report.generateReport(BATCH_PATH);
      } catch (final IOException e) {
        System.out.println("Deletion operation failed");
        LOGGER.error(e.toString());
        LOGGER.info("Deletion operation failed");
      }
    } else {
      System.out.println("Deletion output folder " + deletionOutputPath + " is not empty, please clean the folder before cleaning corpora");
      LOGGER.info("Deletion output folder : " + deletionOutputPath + " is not empty, please clean the folder before cleaning corpora");
    }
  }

  private static void delete(final File file, final String buyerName, final ReportGenerationLogic report, final Ontology ontology,
                             final String decryptOutputPath){
    LOGGER.info("Now cleaning corpus " + file.getName());
    System.out.println("Now cleaning corpus " + file.getName());
    final ArrayList<IDocument> docs = new ArrayList<IDocument>();
    try{
      final CorpusWriter corpusWriter = new CorpusWriter(String.valueOf(Paths.get(decryptOutputPath, file.getName())));
      DocumentFactory.openXmlCorpus(new File(file.getAbsolutePath()), ontology, new DocumentFilter() {
        @Override public void applyTo(final Document document) {
          for (final String personalDataAttribute: PERSONAL_DATA_ATTRIBUTES){
            final String personalDataToDelete = document.getIMetadata().get(personalDataAttribute, "");
            if (!personalDataToDelete.isEmpty() && personalDataToDelete.equalsIgnoreCase(buyerName)) {
              document.getIMetadata().put(personalDataAttribute, RIGHT_TO_BE_FORGOTTEN);
              report.addRow(document, personalDataToDelete, file.getName());
            }
          }
          docs.add(document);
        }
      });
      corpusWriter.write(docs);
      corpusWriter.close();
    } catch (final IOException e) {
      LOGGER.info("Deletion operation failed");
      throw new IllegalStateException(e);
    }
    System.out.println("Completed elaboration of corpus " + file.getName());
    LOGGER.info("Completed cleaning of corpus " + file.getName());
  }


  private static boolean checkIfFolderIsEmpty(final String folderPath) {
    final File outputFolder = new File(folderPath);
    return Objects.requireNonNull(outputFolder.listFiles()).length <= 0;
  }

  private static void checkInputOutputParams(final Element inputEncryptionElement, final Element outputEncryptionElement, final Element inputDecryptionElement, final Element outputDecryptionElement, final Element inputDeletionElement, final Element outputDeletionElement) {
    if (inputEncryptionElement == null){
      throw new IllegalStateException("Encryption input parameter not found in the configuration file");
    }
    if (outputEncryptionElement == null){
      throw new IllegalStateException("Encryption output parameter not found in the configuration file");
    }
    if (inputDecryptionElement == null){
      throw new IllegalStateException("Decryption input parameter not found in the configuration file");
    }
    if (outputDecryptionElement == null){
      throw new IllegalStateException("Decryption output parameter not found in the configuration file");
    }
    if (inputDeletionElement == null){
      throw new IllegalStateException("Deletion input parameter not found in the configuration file");
    }
    if (outputDeletionElement == null){
      throw new IllegalStateException("Deletion output parameter not found in the configuration file");
    }
  }

  private static void checkParameters(final XMLDocument xml, final Element ontoElement, final Element encryptionElement, final Element decryptionElement, final Element deletionElement) {
    if (ontoElement == null){
      throw new IllegalStateException("Ontology parameter not found in the configuration file:" + xml.getFileName());
    }
    if (encryptionElement == null){
      throw new IllegalStateException("Encryption parameter not found in the configuration file:" + xml.getFileName());
    }
    if (decryptionElement == null){
      throw new IllegalStateException("Decryption parameter not found in the configuration file:" + xml.getFileName());
    }
    if (deletionElement == null){
      throw new IllegalStateException("Deletion parameter not found in the configuration file:" + xml.getFileName());
    }
  }

  private static void checkPaths(final String ontoPath, final String encryptInputPath, final String encryptOutputPath, final String decryptInputPath,
                                 final String decryptOutputPath, final String deletionInputPath, final String deletionOutputPath) {
    if (ontoPath == null){
      throw new IllegalStateException("Ontology path not found in the configuration file");
    }
    if (encryptInputPath == null){
      throw new IllegalStateException("Encryption input directory path not found in the configuration file");
    }
    checkIfDirectoryExists(encryptInputPath);
    if (encryptOutputPath == null){
      throw new IllegalStateException("Encryption output directory path not found in the configuration file");
    }
    checkIfDirectoryExists(encryptOutputPath);
    if (decryptInputPath == null){
      throw new IllegalStateException("Decryption input directory path not found in the configuration file");
    }
    checkIfDirectoryExists(decryptInputPath);
    if (decryptOutputPath == null){
      throw new IllegalStateException("Decryption output directory input path not found in the configuration file");
    }
    checkIfDirectoryExists(decryptOutputPath);
    if (deletionInputPath == null){
      throw new IllegalStateException("Deletion input directory input path not found in the configuration file");
    }
    checkIfDirectoryExists(deletionInputPath);
    if (deletionOutputPath == null){
      throw new IllegalStateException("Deletion output directory input path not found in the configuration file");
    }
    checkIfDirectoryExists(deletionOutputPath);
  }

  private static void checkIfDirectoryExists(final String dirPath) {
    final File filesDirectory = new File(dirPath);
    if (!filesDirectory.exists()) {
      if (!filesDirectory.mkdirs()) {
        throw new IllegalStateException("Couldn't create directory, check permissions on server");
      }
    }
  }

}
