package unicredit.gdpr;

import com.acplab.documents.Document;
import com.acplab.documents.views.Metadata;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportGenerationLogic {

  private static final Logger LOGGER = Logger.getLogger(ReportGenerationLogic.class);
  private final List<String[]> reportRows = new ArrayList<>();
  private static final String[] HEADER = {"PO NUMBER", "PO DESCRIPTION", "COMPANY", "DOCUMENT DATE", "BUYERNAME", "SOURCE CORPUS"};

  ReportGenerationLogic(){

  }

  void addRow(final Document document, final String personalDataAttribute, final String corpusName){
      final String[] row = new String[HEADER.length];
      final Metadata metadata = document.getMetadata();
      row[0] = metadata.get("PO_NUMBER", "");
      row[1] = metadata.get("PO_Description", "");
      row[2] = metadata.get("Company", "");
      row[3] = metadata.get("4_SDM_Date", "");
      row[4] = personalDataAttribute;
      row[5] = corpusName;
      reportRows.add(row);
  }

  void generateReport(final String batchPath){
    System.out.println("Now generationg csv report");
    LOGGER.info("Now generationg csv report");
    final Timestamp currentTimestamp = new Timestamp(new Date().getTime());
    final String tsExtraction = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(currentTimestamp);
    final String reportPath = batchPath.concat(File.separator).concat("reports");
    final String filename = reportPath.concat(File.separator).concat("deletionReport_")
                                                             .concat(tsExtraction)
                                                             .concat(".csv");
    PrintWriter printWriter = null;
    try {
      final File f = new File(filename);
      final String eol = "\r\n";
      final FileOutputStream file = new FileOutputStream(f.getAbsolutePath());
      final OutputStreamWriter outputStream = new OutputStreamWriter(file, StandardCharsets.UTF_8);
      printWriter = new PrintWriter(outputStream);

      String head = "";
      for (int i = 0; i < HEADER.length; i++) {
        if (i == HEADER.length - 1) {
          head = head.concat(HEADER[i]);
        } else {
          head = head.concat(HEADER[i]).concat(";");
        }
      }
      printWriter.print(head);
      printWriter.print(eol);

      String rowNormalized;
      for (final String[] row : reportRows) {
        rowNormalized = "";
        for (int i = 0; i < row.length; i++) {
          if (i == row.length - 1) {
            rowNormalized = rowNormalized.concat(row[i]);
          } else {
            rowNormalized = rowNormalized.concat(row[i]).concat(";");
          }
        }
        printWriter.print(rowNormalized);
        printWriter.print(eol);
      }
    } catch (final Exception e) {
      throw new IllegalStateException(e);
    } finally {
      if (printWriter != null) {
        printWriter.close();
      }
    }
    System.out.println("Csv report available at " + reportPath);
    LOGGER.info("Csv report available at " + reportPath);
  }

}
